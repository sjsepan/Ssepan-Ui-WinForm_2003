# readme.md - README for Ssepan.Ui.WinForm v6.0

## About

Common library of application functions for C# applications, specific to Guis (WinForms) UIs; requires ssepan.application, ssepan.io, ssepan.utility
Ssepan.Ui.Mono.WinForm back-ported to Visual C# 2003 and .Net 1.1. Note: Going from later version requires giving up several features.
~ No Action type; used delegates
~ No generics; used overloads
~ No LINQ; used for and if/else
~ Different types for menu and toolbar; used ImageList for toolbar images
~ No 'default' keyword; used explicit nulls, enums, or values

### Purpose

To encapsulate common application functionality, reduce custom coding needed to start a new project, and provide consistency across projects.

### Usage notes

~...

### History

6.0:
~initial release

Steve Sepan
ssepanus@yahoo.com
9/27/2022
