﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Windows.Forms;
using Ssepan.Utility;
using Ssepan.Io;
using Ssepan.Application;
//using Microsoft.Data.ConnectionUI;//cannot use until Microsoft.VisualStudio.Data.dll is made redistributable by Microsoft
using System.Reflection;

namespace Ssepan.Ui.WinForm
{
    public class Dialogs
    {
        #region Declarations
        #endregion Declarations

        #region Methods
        /// <summary>
        /// Get path to save data.
        /// Static. Calls SaveFileDialog.
        /// </summary>
        /// <param name="fileDialogInfo"></param>
        /// <param name="errorMessage"></param>
        public static Boolean GetPathForSave
        (
            ref FileDialogInfo fileDialogInfo, 
            ref String errorMessage
        )
        {
            Boolean returnValue = false;
            SaveFileDialog saveFileDialog = null;
            DialogResult response = DialogResult.None;
            String messageTemp = null;

            try
            {
                if 
                (
                    (fileDialogInfo.Filename.EndsWith(fileDialogInfo.NewFilename)) 
                    || 
                    (fileDialogInfo.ForceDialog)
                )
                {
                    using 
                    (
                        saveFileDialog = new SaveFileDialog()
                    )
                    {
                        saveFileDialog.Title = fileDialogInfo.Title;//(fileDialogInfo.ForceDialog ? "Save As..." : "Save...")
                        // saveFileDialog.CheckFileExists = ?;

                        //define location of file for settings by prompting user for filename.
                        saveFileDialog.FileName = fileDialogInfo.Filename;

                        saveFileDialog.Filter = fileDialogInfo.Filters;

                        saveFileDialog.InitialDirectory = 
                            (fileDialogInfo.InitialDirectory == Environment.SpecialFolder.Personal) 
                            ? 
                            fileDialogInfo.CustomInitialDirectory 
                            : 
                            PathExtensions.WithTrailingSeparator(Environment.GetFolderPath(Environment.SpecialFolder.Personal));

                        response = (DialogResult)saveFileDialog.ShowDialog(fileDialogInfo.Parent); 

                        if (response != DialogResult.None)
                        {
                            if (response == DialogResult.OK)
                            {
                                if (Path.GetFileName(saveFileDialog.FileName).ToLower() == (fileDialogInfo.NewFilename).ToLower())
                                {
                                    //user did not select or enter a name different than new; for now I have chosen not to allow that name to be used for a file.--SJS, 12/16/2005
                                    messageTemp = String.Format("The name \"{0}.{1}\" is not allowed; please choose another. Settings not saved.", fileDialogInfo.NewFilename.ToLower(), fileDialogInfo.Extension.ToLower());
                                    MessageDialogInfo messageDialogInfo = 
                                        new MessageDialogInfo
                                        (
                                            /*parent:*/ fileDialogInfo.Parent,
                                            /*modal:*/ fileDialogInfo.Modal,
                                            /*title:*/ fileDialogInfo.Title,
                                            /*dialogFlags:*/ null,
                                            /*messageType:*/ MessageBoxIcon.Information,
                                            /*buttonsType:*/ MessageBoxButtons.OK,
                                            /*message:*/ messageTemp,
                                            /*response:*/ DialogResult.None
                                        );

                                    ShowMessageDialog
                                    (
                                        ref messageDialogInfo,
                                        ref errorMessage
                                    ); 
                                    
                                    //Forced cancel
                                    fileDialogInfo.Response = DialogResult.Cancel;
                                }
                                else
                                {
                                    //set new filename
                                    fileDialogInfo.Filename = saveFileDialog.FileName;
                                    // fileDialogInfo.Filenames = saveFileDialog.Filenames;
                                }
                            }
                            fileDialogInfo.Response = response;
                            returnValue = true;
                        }
                    }
                }
                else
                {
                    fileDialogInfo.Response = DialogResult.OK;
                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
            }

            fileDialogInfo.BoolResult = returnValue;
            return returnValue;
        }

        /// <summary>
        /// Get path to load data.
        /// Static. Calls OpenFileDialog.
        /// </summary>
        /// <param name="fileDialogInfo"></param>
        /// <param name="errorMessage"></param>
        /// <param name="forceNew"></param>
        public static Boolean GetPathForLoad
        (
            ref FileDialogInfo fileDialogInfo, 
            ref String errorMessage
        )
        {
            Boolean returnValue = false;
            OpenFileDialog openFileDialog = null;
            DialogResult response = DialogResult.None;

            try
            {
                if (fileDialogInfo.ForceNew)
                {
                    fileDialogInfo.Filename = fileDialogInfo.NewFilename;

                    returnValue = true;
                }
                else
                {
                    using 
                    (
                        openFileDialog = new OpenFileDialog()
                    )
                    {
                        openFileDialog.Title = fileDialogInfo.Title;
                        // fileChooserDialog.CheckFileExists = ?;

                        //define location of file for settings by prompting user for filename.
                        openFileDialog.Filter = fileDialogInfo.Filters;
                        
                        openFileDialog.Multiselect = fileDialogInfo.Multiselect;
                        openFileDialog.FileName = fileDialogInfo.Filename;
                        openFileDialog.InitialDirectory =
                                (fileDialogInfo.InitialDirectory == Environment.SpecialFolder.Personal)
                                ?
                                fileDialogInfo.CustomInitialDirectory
                                :
                                PathExtensions.WithTrailingSeparator(Environment.GetFolderPath(Environment.SpecialFolder.Personal));

                        response = (DialogResult)openFileDialog.ShowDialog(fileDialogInfo.Parent); 

                        if (response != DialogResult.None)
                        {
                            if (response == DialogResult.OK)
                            {
                                fileDialogInfo.Filename = openFileDialog.FileName;
                                fileDialogInfo.Filenames = (String[])openFileDialog.FileNames;
                            }
                            fileDialogInfo.Response = response;
                            returnValue = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                    
                throw;
            }

            fileDialogInfo.BoolResult = returnValue;
            return returnValue;
        }

        /// <summary>
        /// Select a folder path.
        /// Static. Calls FolderBrowserDialog.
        /// </summary>
        /// <param name="fileDialogInfo">returns path in Filename property</param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static Boolean GetFolderPath
        (
            ref FileDialogInfo fileDialogInfo,
            ref String errorMessage
        )
        {
            Boolean returnValue = false;
            DialogResult response = DialogResult.None;
            FolderBrowserDialog folderBrowserDialog = null;

            try
            {
                folderBrowserDialog = new FolderBrowserDialog();
                // folderBrowserDialog.Title = fileDialogInfo.Title;
                folderBrowserDialog.SelectedPath = 
                (
                    StringExtensions.IsNullOrEmpty(fileDialogInfo.Filename) 
                    ? 
                    PathExtensions.WithTrailingSeparator(Environment.GetFolderPath(Environment.SpecialFolder.Personal)) 
                    : 
                    fileDialogInfo.Filename
                );

                response = (DialogResult)folderBrowserDialog.ShowDialog(fileDialogInfo.Parent); 

                if (response != DialogResult.None)
                {
                    if (response == DialogResult.OK)
                    {
                        fileDialogInfo.Filename = folderBrowserDialog.SelectedPath;
                    }
                    fileDialogInfo.Response = response;
                    returnValue = true;
                }

                folderBrowserDialog.Dispose();
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
            }

            fileDialogInfo.BoolResult = returnValue;
            return returnValue;
        }

        /// <summary>
        /// Shows an About dialog.
        /// Static. Calls .AboutDialog.
        /// </summary>
        /// <param name="aboutDialogInfo"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static Boolean ShowAboutDialog
        (
            ref AboutDialogInfo aboutDialogInfo,
            ref String errorMessage
        )
        {
            Boolean returnValue = false;
            DialogResult response = DialogResult.None;
            String message = null;

            try
            {
                message += String.Format("ProgramName: {0}\n", aboutDialogInfo.ProgramName);
                message += String.Format("Version: {0}\n", aboutDialogInfo.Version);
                message += String.Format("Description: {0}\n", aboutDialogInfo.Comments);
                message += String.Format("WebsiteLabel: {0}\n", aboutDialogInfo.WebsiteLabel);
                message += String.Format("Website: {0}\n", aboutDialogInfo.Website);
                // // aboutDialog.Platform;r/o
                message += String.Format("Copyright: {0}\n", aboutDialogInfo.Copyright);
                message += String.Format("Designers: {0}\n", aboutDialogInfo.Designers);
                message += String.Format("Developers: {0}\n", aboutDialogInfo.Developers);
                message += String.Format("Documenters: {0}\n", aboutDialogInfo.Documenters);
                message += String.Format("License: {0}\n", aboutDialogInfo.License);

                response = MessageBox.Show
                (
                    /*owner:*/ aboutDialogInfo.Parent, //this,
                    /*text:*/ message,
                    /*caption:*/ aboutDialogInfo.Title, //"About " + this.Text,
                    /*buttons:*/ MessageBoxButtons.OK,
                    /*icon:*/ MessageBoxIcon.Information,
                    /*defaultButton:*/ MessageBoxDefaultButton.Button1
                );
                
                if (response != DialogResult.None)
                {
                    //if (response == DialogResult.OK)
                    // {

                    // }
                    aboutDialogInfo.Response = response;
                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
            }

            aboutDialogInfo.BoolResult = returnValue;
            return returnValue;
        }

        /// <summary>
        /// Get a printer.
        /// Static. 
        /// </summary>
        /// <param name="printerDialogInfo">PrinterDialogInfo</param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static Boolean GetPrinter
        (
            ref PrinterDialogInfo printerDialogInfo,
            ref String errorMessage
        )
        {
            Boolean returnValue = false;
            DialogResult response = DialogResult.None;
            PrintDialog printDialog = null;
            
            try
            {
                printDialog = new PrintDialog();
                printDialog.PrinterSettings = new PrinterSettings();
                
                response = printDialog.ShowDialog(printerDialogInfo.Parent);
                
                if (response != DialogResult.None)
                {
                    if (response == DialogResult.OK)
                    {
                        printerDialogInfo.Printer = printDialog.PrinterSettings;
                    }

                    printerDialogInfo.Response = response;
                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
            }
            finally
            {
                printDialog.Dispose();
            }
            
            printerDialogInfo.BoolResult = returnValue;
            return returnValue;
        }
        /// <summary>
        /// Shows a message dialog.
        /// Static. 
        /// </summary>
        /// <param name="messageDialogInfo"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static Boolean ShowMessageDialog
        (
            ref MessageDialogInfo messageDialogInfo,
            ref String errorMessage
        )
        {
            Boolean returnValue = false;
            DialogResult response = DialogResult.None;

            try
            {
                response = MessageBox.Show
                (
                    /*owner:*/ messageDialogInfo.Parent,
                    /*text:*/ messageDialogInfo.Message,
                    /*caption:*/ messageDialogInfo.Title,
                    /*buttons:*/ messageDialogInfo.ButtonsType,
                    /*icon:*/ messageDialogInfo.MessageType,
                    /*defaultButton:*/ MessageBoxDefaultButton.Button1
                );

                if (response != DialogResult.None)
                {
                    //return complex responses in dialoginfo
                    messageDialogInfo.Response = response;
                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
            }
            
            messageDialogInfo.BoolResult = returnValue;
            return returnValue;
        }

        /// <summary>
        /// Get a color.
        /// </summary>
        /// <param name="colorDialogInfo">ColorDialogInfo</param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static Boolean GetColor
        (
            ref ColorDialogInfo colorDialogInfo,
            ref String errorMessage
        )
        {
            Boolean returnValue = false;
            ColorDialog colorDialog = null;
            DialogResult response = DialogResult.None;

            try
            {
                colorDialog = new ColorDialog();
                colorDialog.Color = colorDialogInfo.Color;

                response = colorDialog.ShowDialog(colorDialogInfo.Parent);

                if (response != DialogResult.None)
                {
                    if (response == DialogResult.OK)
                    {
                        colorDialogInfo.Color = colorDialog.Color;
                    }
                    colorDialogInfo.Response = response;
                    returnValue = true;
                }
            
                colorDialog.Dispose();
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
            }
            
            colorDialogInfo.BoolResult = returnValue;
            return returnValue;
        }

        /// <summary>
        /// Get a font.
        /// </summary>
        /// <param name="fontDescription"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static Boolean GetFont
        (
            ref FontDialogInfo fontDialogInfo,
            ref String errorMessage
        )
        {
            Boolean returnValue = false;
            FontDialog fontDialog = null;
            DialogResult response = DialogResult.None;

            try
            {
                // fontResponse = null;
                fontDialog = new FontDialog();
                fontDialog.Font = fontDialogInfo.FontDescription;
                fontDialog.Color = fontDialogInfo.ColorDescription;

                response = fontDialog.ShowDialog(fontDialogInfo.Parent);
                // Console.WriteLine("GetFont:response="+response.ToString());
                if (response != DialogResult.None)
                {
                    if (response == DialogResult.OK)
                    {
                        fontDialogInfo.FontDescription = fontDialog.Font;
                    }
                    fontDialogInfo.Response = response;
                    returnValue = true;
                }//closing dialog window treated as Cancel
            
                fontDialog.Dispose();
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
            }
            
            fontDialogInfo.BoolResult = returnValue;
            return returnValue;
        }


        /// <summary>
        /// Perform input of connection String and provider name.
        /// Uses MS Data Connections Dialog.
        /// Note: relies on MS-LPL license and code from http://archive.msdn.microsoft.com/Connection
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="providerName"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static Boolean GetDataConnection
        (
            ref String connectionString,
            ref String providerName,
            ref String errorMessage
        )
        {
            Boolean returnValue = false;
            //DataConnectionDialog dataConnectionDialog = default(DataConnectionDialog);
            //DataConnectionConfiguration dataConnectionConfiguration = default(DataConnectionConfiguration);

            try
            {
                //dataConnectionDialog = new DataConnectionDialog();

                //DataSource.AddStandardDataSources(dataConnectionDialog);

                //dataConnectionDialog.SelectedDataSource = DataSource.SqlDataSource;
                //dataConnectionDialog.SelectedDataProvider = DataProvider.SqlDataProvider;

                //dataConnectionConfiguration = new DataConnectionConfiguration(null);
                //dataConnectionConfiguration.LoadConfiguration(dataConnectionDialog);

                //(don't) set to current connection string, because it overwrites previous settings, requiring user to click Refresh in Data Connection Dialog.
                //dataConnectionDialog.ConnectionString = connectionString;

                if (true/*DataConnectionDialog.Show(dataConnectionDialog) == DialogResult.OK*/)
                {
                    ////extract connection string
                    //connectionString = dataConnectionDialog.ConnectionString;
                    //providerName = dataConnectionDialog.SelectedDataProvider.ViewName;

                    ////writes provider selection to xml file
                    //dataConnectionConfiguration.SaveConfiguration(dataConnectionDialog);

                    ////save these too
                    //dataConnectionConfiguration.SaveSelectedProvider(dataConnectionDialog.SelectedDataProvider.ToString());
                    //dataConnectionConfiguration.SaveSelectedSource(dataConnectionDialog.SelectedDataSource.ToString());

                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

            }
            return returnValue;
        }
        #endregion Methods
    }
}
